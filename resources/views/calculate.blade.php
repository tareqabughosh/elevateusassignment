<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Calculator</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
<style>
    .form-control{
        width: 80%;
    }
</style>
</head>
<body>
<div class="m-4 container">
    @if(session('success') || session('error'))
    <div class="container toasts-messages">
        <div class="row">
            @if(session('success'))
            <div class="col-md-8 mx-auto alert alert-success" role="alert">
                {{session('success')}}
            </div>
            @endif
            @if(session('error'))
            <div class="col-md-8 mx-auto alert alert-danger" role="alert">
                {{session('error')}}
            </div>
            @endif
        </div>
    </div>
    @endif
    <form action="{{route('calculate')}}" method="post" class="">
        @csrf
        <div class="col-md-8 mx-auto ">
            <label class="form-label" for="first">First sentence/word</label>
            <input type="text" class="form-control" id="first" name="first" required>
            <label class="form-label" for="second">Second sentence/word</label>
            <input type="text" class="form-control" id="second" name="second" required>
            <button type="submit" class="btn btn-primary">Calculate</button>
        </div>
    </form>
    <br>
    @if(session('res'))
    <div class="col-md-8 mx-auto alert alert-success" role="alert">
       The result is: {{session('res')}}
    </div>
    @endif
</div>
</body>
</html>