<?php
namespace App\abstracts;

abstract class distanceAbstract
{
    protected $var1, $var2;

    public function __construct($var1, $var2)
    {
        $this->var1 = $var1;
        $this->var2 = $var2;
    }
}