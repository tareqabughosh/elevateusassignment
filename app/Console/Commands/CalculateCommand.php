<?php

namespace App\Console\Commands;

use App\Helper\Calculator;
use Illuminate\Console\Command;

class CalculateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:calculate {first} {second}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To calculate hamming distance or levenshtein';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $first = $this->argument('first');
        $second = $this->argument('second');
        if (is_string($first) !== true || is_string($second) !== true) {
            return $this->error('Please type a string');
        }
        $this->info("The result is: " . Calculator::calculateDistance($first, $second));
    }
}
