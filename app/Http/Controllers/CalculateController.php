<?php

namespace App\Http\Controllers;

use App\Helper\Calculator;
use Illuminate\Http\Request;

class CalculateController extends Controller
{
    public function calculate(Request $request)
    {
        if (is_string($request->first) !== true || is_string($request->second) !== true) {
            return back()->with('error', 'Please type a text');
        }

        $res = Calculator::calculateDistance($request->first, $request->second);

        return back()->with('res', $res);
    }
}
