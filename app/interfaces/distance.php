<?php
namespace App\interfaces;

interface distance
{
    public function calculate(): int;
}