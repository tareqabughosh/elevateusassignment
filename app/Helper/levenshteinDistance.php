<?php

namespace App\Helper;

use App\interfaces\distance;
use App\abstracts\distanceAbstract;

class levenshteinDistance extends distanceAbstract implements distance
{
    public function calculate(): int
    {
        // using builtin php function to calculate levenshtein
        return levenshtein($this->var1, $this->var2);
    }
}
