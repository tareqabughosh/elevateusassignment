<?php

namespace App\Helper;
use App\interfaces\distance;
use App\abstracts\distanceAbstract;

class hammingDistance extends distanceAbstract implements distance
{
    public function calculate(): int
    {
        //turning the strings into arrays and then comparing them to each other, since they are the same length, any character that doesnt exist from $first to $second will be counted 
        return count(array_diff_assoc(str_split($this->var1), str_split($this->var2)));
    }
}
