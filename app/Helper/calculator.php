<?php

namespace App\Helper;

class Calculator
{
    public static function calculateDistance($first, $second)
    {
        if (strlen($first) === strlen($second)
        ) {
            $res = new hammingDistance($first, $second);
        } else {
            $res = new levenshteinDistance($first, $second);
        }

        return $res->calculate();
    }
}
